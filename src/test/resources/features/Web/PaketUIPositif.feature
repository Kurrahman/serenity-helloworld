@UI @Paket @Positive
Feature: UI Paket data positif

	Background: The user is logged in
		Given The user is on the digital product page
		And The Login cookie is loaded to the browser

	@WebA
	Scenario Outline: Filling number and continue
		When The user fill <handphone> to the form
		And The user change product to paket data
		And The user click on pay now button
		Then The checkout number should be <handphone2>
		Examples:
			| handphone    | handphone2     |
			| 08984897050  | 0898 4897 050  |
			| 081357263067 | 0813 5726 3067 |

	@WebB @Point
	Scenario Outline: Filling number and change product
		When The user fill <handphone> to the form
		And The user change product to paket data
		And The user change the product to <productName>
		And The user click on pay now button
		Then The checkout amount should be <productName>
		Examples:
			| handphone    | productName |
			| 081357263067 | GameMax 3GB |
			| 08984897050  | AON 6GB     |

	@WebB
	Scenario Outline: Filling number and change number
		When The user fill <handphone1> to the form
		And The user fill <handphone2> to the form
		And The user change product to paket data
		And The user click on pay now button
		Then The checkout number should be <handphone3>
		Examples:
			| handphone1   | handphone2   | handphone3     |
			| 08984897050  | 081357263067 | 0813 5726 3067 |
			| 081357263067 | 08984897050  | 0898 4897 050  |

	@WebB
	Scenario Outline: Filling number and change partially
		When The user fill <handphone1> to the form
		And The user change <ndigit> from position <position> to <change>
		And The user change product to paket data
		And The user click on pay now button
		Then The checkout number should be <handphone2>
		Examples:
			| handphone1  | ndigit | position | change  | handphone2    |
			| 08984897050 | 3      | 2        | 135     | 0813 5897 050 |
			| 08984897050 | 7      | 2        | 0000000 | 0800 0000 050 |