@UI @Pulsa @Negative
Feature: UI Pulsa negatif

	@WebA
	Scenario Outline: Filling invalid number and continue
		Given The user is on the digital product page
		And The Login cookie is loaded to the browser
		When The user fill <handphone> to the form
		And The user submit the number
		Then The user should see an error message
		Examples:
			| handphone        |
			| 0000000000000000 |
			| 08135            |
			|                  |
			| 0808080808       |