@Mobile @MobilePulsa @Negative
Feature: Test case pulsa negatif

	Background: Login and navigate to Pulsa Page
		Given User open Blibli application
		And User click login using google
		And User select the topmost google account
		And User click on all category button
		And User click on "Pulsa" Category

	@MobileA
	Scenario Outline: Filling invalid number and continue
		When User fill <number> to the form
		And User click on pay now button
		Then User should see an error message
		Examples:
			| number    |
			| 000000000 |
#			| 08135     |
#			|           |
			| 080808080 |