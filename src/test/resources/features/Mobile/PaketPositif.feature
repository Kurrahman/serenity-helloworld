@Mobile @MobilePaket @Positive
Feature: Test case paket data positif

	Background: Login and navigate to Paket Page
		Given User open Blibli application
		And User click login using google
		And User select the topmost google account
		And User click on all category button
		And User click on "Paket Data" Category

	@MobileA
	Scenario Outline: Filling number and continue
		When User fill <handphone> to the form
		And User click on pay now button
		Then Checkout number should be <handphone>
		Examples:
			| handphone    |
			| 08984897050  |
			| 081357263067 |

	@MobileB
	Scenario Outline: Filling number and change product
		When User fill <handphone> to the form
		And User click on products dropdown
		When User change the product to <productName>
		And User click on pay now button
		Then Checkout product name should be <productName>
		Examples:
			| handphone    | productName |
			| 081357263067 | GameMax 3GB |
			| 08984897050  | AON 6GB     |

	@MobileB
	Scenario Outline: Filling number and change number
		When User fill <handphone1> to the form
		And User fill <handphone2> to the form
		And User click on pay now button
		Then Checkout number should be <handphone2>
		Examples:
			| handphone1   | handphone2   |
			| 08984897050  | 081357263067 |
			| 081357263067 | 08984897050  |