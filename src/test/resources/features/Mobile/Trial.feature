@Mobil @Sandbox
Feature: Mobile UI Sandbox

	Background: Login
		Given User open Blibli application
		And User click login using google
		And User select the topmost google account
		And User click on all category button
		And User click on "Paket Data" Category

	Scenario Outline: Filling number and change product
		When User fill <handphone> to the form
		And User click on products dropdown
		When User change the product to <productName>
		And User click on pay now button
		Then Checkout product name should be <productName>
		Examples:
			| handphone    | productName |
			| 081357263067 | GameMax 3GB |