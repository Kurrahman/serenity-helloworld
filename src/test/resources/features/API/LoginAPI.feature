@LoginAPI
Feature: Logging in

	Scenario: Logging in using API and writing cookies to a file
		Given User login using "akutakbiasa36@gmail.com" with password "random123"
		And User request challenge code
		And User retrieve OTP code from "akutakbiasa36@gmail.com" email inbox
		When User submit the OTP code
		Then User should be logged in