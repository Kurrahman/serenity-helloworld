@API @Pulsa
Feature: API Pulsa

	@Positive
	Scenario Outline: Getting response for a valid number
		When User request for phone credit for <phoneNumber>
		Then The number response should be 200 OK
		And The provider should be <providerName>
		Examples:
			| phoneNumber  | providerName |
			| 0898         | 3            |
			| 081357263067 | Telkomsel    |
			| 085555555555 | Indosat      |


	Scenario Outline: Adding an order to cart
		Given User load cookies from file
		When User add order <customerNumber>, <sku>, <operatorName>, and "PHONE_CREDIT" to cart
		Then The add to cart response should be 200 OK
		And The sku added should be <sku>
		Examples:
			| customerNumber | sku                   | operatorName |
			| 08984897050    | BLP-25978-XDP-0000318 | 3            |
			| 081357263067   | BLP-25978-XDP-0000306 | Telkomsel    |


	@Checkout
	Scenario Outline: Making a credit purchase
		Given User load cookies from file
		When User add order <customerNumber>, <sku>, <operatorName>, and "PHONE_CREDIT" to cart
		And User checkout the current cart
		Then The checkout response should be 200 OK
		And An id for the order is generated
		Examples:
			| customerNumber | sku                   | operatorName |
			| 08984897050    | BLP-25978-XDP-0000318 | 3            |
			| 081357263067   | BLP-25978-XDP-0000306 | Telkomsel    |

	@Negative
	Scenario Outline: Getting response for an invalid number
		When User request for phone credit for <phoneNumber>
		Then The number response should be 429 TOO_MANY_REQUEST
		Examples:
			| phoneNumber         |
			|                     |
			| 000000000000        |
			| randomnumber        |
			| 089                 |
			| 0898489705070507050 |
