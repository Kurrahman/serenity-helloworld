package org.example.utility;

import org.jsoup.Jsoup;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import java.io.FileReader;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckingEmail {
    public static String getVerificationCode(String username, String password) throws Exception {
        // Load Email properties
        Properties accProp = new Properties();
        FileReader reader = new FileReader("src/test/resources/data/gmail.properties");
        accProp.load(reader);

        // Setting up JavaMail
        Properties emailProp = new Properties();
        emailProp.put("mail.pop3.host", accProp.getProperty("emailHost"));
        emailProp.put("mail.pop3.port", "995");
        emailProp.put("mail.pop3.starttls.enable", "true");
        Session emailSession = Session.getDefaultInstance(emailProp);

        // Getting email from GMail inbox
        Store store = emailSession.getStore("pop3s");
        store.connect(accProp.getProperty("emailHost"), username, password);

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_ONLY);

        // Picking latest verification email from inbox
        Message[] messages = folder.getMessages();
        String emailContent = "";
        for (int i = messages.length - 1; i >= 0; i--) {
            if (messages[i].getFrom()[0].toString().equals(accProp.getProperty("emailSender"))) {
                if (messages[i].getSubject().equals(accProp.getProperty("emailSubject"))) {
                    emailContent = messages[i].getContent().toString();
                    break;
                }
            }
        }

        // Parsing email content to extract verification number
        String verificationCode = Jsoup.parse(emailContent).text();
        verificationCode = verificationCode.replaceAll(" ", "");

        Pattern pattern = Pattern.compile("\\d{4}");
        Matcher matcher = pattern.matcher(verificationCode);

        if (matcher.find()) {
            verificationCode = matcher.group(0);
        }

        return verificationCode;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(getVerificationCode("akutakbiasa36@gmail.com", "Kolong123"));
    }
}
