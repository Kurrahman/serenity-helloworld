package org.example.utility;

import io.restassured.http.Cookie;

import java.io.*;
import java.util.*;

public class TestResults {

    public static final String API_RESULTS_DIRECTORY = "src/test/resources/data/Api-Results.data";
    public static final String WEB_RESULTS_DIRECTORY = "src/test/resources/data/Web-Results.data";
    public static final String MOBILE_RESULTS_DIRECTORY = "src/test/resources/data/Mobile-Results.data";

    public static void resetOutput(){
        File file0 = new File(API_RESULTS_DIRECTORY);
        File file1 = new File(WEB_RESULTS_DIRECTORY);
        File file2 = new File(MOBILE_RESULTS_DIRECTORY);
        try {
            // Delete old file if exists
            for (File file : Arrays.asList(file0, file1, file2)) {
                file.delete();
                file.createNewFile();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void writeResult(String fileDir, String name, String result){
        File file = new File(fileDir);
        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(name+";");
            if (result==null){
                bufferedWriter.write("null");
            }
            else {
                bufferedWriter.write(result);
            }
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> readResults(String fileDir){
        File file = new File(fileDir);
        try {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
            Map<String, String> results = new HashMap<>();

            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strLine, ";");
                results.put(token.nextToken(),token.nextToken());
            }
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
