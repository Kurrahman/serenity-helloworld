package org.example.utility;

import io.restassured.http.Cookies;
import io.restassured.http.Cookie;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class CookieLoader {
    private static final String[] COOKIES_NAME = {"GDN-QA", "Blibli-User-Id", "Blibli-Is-Member", "Blibli-Is-Remember", "Blibli-Session-Id", "Blibli-Signature"};
    public static final String COOKIE_QA_DIRECTORY = "src/test/resources/data/cookies-qa.data";
    public static final String COOKIE_LOGIN_DIRECTORY = "src/test/resources/data/cookies-login.data";

    public static Cookie getQAKey() {
        try {
            File file = new File(COOKIE_QA_DIRECTORY);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strLine, ";");
                return new Cookie.Builder(token.nextToken(), token.nextToken()).
                        setDomain(token.nextToken()).
                        setPath(token.nextToken()).
                        setSecured(Boolean.parseBoolean(token.nextToken())).
                        build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void loadCookie(String cookieFile, WebDriver driver) {
        try {
            File file = new File(cookieFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strLine, ";");
                while (token.hasMoreTokens()) {
                    String name = token.nextToken();
                    String value = token.nextToken();
                    String domain = token.nextToken();
                    String path = token.nextToken();
                    Date expiry = null;

                    String val;
                    if (!(val = token.nextToken()).equals("null")) {
                        expiry = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy").parse(val);
                    }
                    boolean isSecure = Boolean.parseBoolean(token.nextToken());
                    org.openqa.selenium.Cookie ck = new org.openqa.selenium.Cookie(name, value);
//                    System.out.println(ck);
                    driver.manage().addCookie(ck); // This will add the stored cookie to your current session
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Cookies getLoginCookies() {
        try {
            File file = new File(COOKIE_LOGIN_DIRECTORY);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            List<Cookie> tmpC = new ArrayList<>();

            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strLine, ";");
                tmpC.add(new Cookie.Builder(token.nextToken(), token.nextToken()).
                        setDomain(token.nextToken()).
                        setPath(token.nextToken()).
                        setSecured(Boolean.parseBoolean(token.nextToken())).
                        build());
            }
            return new Cookies(tmpC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeCookies(Cookies cookies) {
        File file = new File(COOKIE_LOGIN_DIRECTORY);
        try {
            // Delete old file if exists
            file.delete();
            file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter Bwrite = new BufferedWriter(fileWrite);

            // loop for getting the cookie information
            for (String name : COOKIES_NAME) {
                Cookie ck = cookies.get(name);
                Bwrite.write((ck.getName() + ";" + ck.getValue() + ";" + ck.getDomain() + ";" + ck.getPath() + ";" + ck.getExpiryDate() + ";" + ck.isSecured()));
                Bwrite.newLine();
            }
            Bwrite.close();
            fileWrite.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
