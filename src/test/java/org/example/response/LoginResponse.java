package org.example.response;

import org.example.response.dataModel.LoginData;

public class LoginResponse extends RequestResponse {
    private LoginData data;

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
}
