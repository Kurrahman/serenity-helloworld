package org.example.response;

import org.example.response.dataModel.GetCartData;

public class GetCartResponse extends RequestResponse {
    private GetCartData data;

    public GetCartData getData() {
        return data;
    }

    public void setData(GetCartData data) {
        this.data = data;
    }
}
