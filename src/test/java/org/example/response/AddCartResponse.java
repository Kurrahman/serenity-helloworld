package org.example.response;

import org.example.response.dataModel.AddCartData;

public class AddCartResponse extends RequestResponse {
    private AddCartData data;

    public AddCartData getData() {
        return data;
    }

    public void setData(AddCartData data) {
        this.data = data;
    }
}
