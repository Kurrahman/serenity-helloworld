package org.example.response.dataModel;

import java.util.List;

public class NumberFormData {
    private String provider;
    private List<Product> products;

    public String getProvider() {
        return provider;
    }

    public List<Product> getProducts() {
        return products;
    }
}
