package org.example.response;

import org.example.response.dataModel.NumberFormData;

public class NumberFormResponse extends RequestResponse {
    private NumberFormData data;

    public NumberFormData getData() {
        return data;
    }

    public void setData(NumberFormData data) {
        this.data = data;
    }
}
