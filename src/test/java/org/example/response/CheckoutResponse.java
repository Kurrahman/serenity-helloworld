package org.example.response;

import org.example.response.dataModel.CheckoutData;

public class CheckoutResponse extends RequestResponse {
    private CheckoutData data;

    public CheckoutData getData() {
        return data;
    }

    public void setData(CheckoutData data) {
        this.data = data;
    }
}
