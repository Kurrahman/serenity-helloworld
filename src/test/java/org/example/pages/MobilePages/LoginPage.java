package org.example.pages.MobilePages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

public class LoginPage extends PageObject {
    @FindBy(id = "blibli.mobile.commerce:id/bt_google_login")
    WebElementFacade googleLoginBtn;

    @FindBy(id = "com.google.android.gms:id/container")
    WebElementFacade googleAccount;

    public void loginByGoogleAccount() {
        googleLoginBtn.
                withTimeoutOf(15, SECONDS).
                click();
    }

    public void selectGoogleAccount() {
        googleAccount.
                withTimeoutOf(15, SECONDS).
                click();
    }
}
