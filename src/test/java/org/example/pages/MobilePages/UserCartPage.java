package org.example.pages.MobilePages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

public class UserCartPage extends PageObject {
    @FindBy(xpath = "//android.widget.Button[@text = 'Belanja sekarang']")
    WebElementFacade backToHomeBtn;

    public void backToHome() {
        backToHomeBtn.
                withTimeoutOf(15, SECONDS).
                click();
    }
}
