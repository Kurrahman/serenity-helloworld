package org.example.pages.MobilePages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.example.utility.AndroidUtility.swipeUp;

public class DigitalPage extends PageObject {
    @FindBy(xpath = "//android.widget.TextView[@text='Tagihan & Isi Ulang']")
    private WebElementFacade title;

    private static final String[] valuePath = {"//android.widget.TextView[@text = '", "']/parent::*"};
    private WebElementFacade selectedValue;

    @FindBy(className = "android.widget.EditText")
    private WebElementFacade phoneNumber;

    @FindBy(id = "blibli.mobile.commerce:id/sp_recharge_list")
    private WebElementFacade valueDropdown;

    @FindBy(className = "android.widget.Button")
    private WebElementFacade checkout;

    @FindBy(id = "blibli.mobile.commerce:id/tv_error_message")
    private WebElementFacade errorMsg;

    public void fillPhoneNumber(String number){
        phoneNumber.withTimeoutOf(10, SECONDS).type(number);
        title.click();
    }

    public void clickValueDropdown(){
        valueDropdown.click();
    }

    public void selectValue(String value){
        selectedValue = find(By.xpath(valuePath[0]+value+valuePath[1]));
        while (!selectedValue.isVisible()){
            swipeUp(300);
        }
        selectedValue.click();
    }

    public void clickCheckoutBtn(){
        while (!checkout.isVisible()){
            swipeUp(500);
        }
        checkout.click();
        waitABit(3000);
    }

    public boolean isErrorVisible(){
        return errorMsg.withTimeoutOf(10,SECONDS).isVisible();
    }
}
