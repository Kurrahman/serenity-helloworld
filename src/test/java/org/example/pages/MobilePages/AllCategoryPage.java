package org.example.pages.MobilePages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

public class AllCategoryPage extends PageObject {

    private static final String[] pulsaLocator = {"//android.widget.TextView[@text='","']/parent::*"};
    private WebElementFacade pulsaBtn;

    public void clickCategoryBtn(String category){
        pulsaBtn = find(By.xpath(pulsaLocator[0]+category+pulsaLocator[1]));
        System.out.println(pulsaBtn.getCoordinates().toString());
        pulsaBtn.withTimeoutOf(10, SECONDS).click();
    }
}
