package org.example.pages.MobilePages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

public class HomePage extends PageObject {
    @FindBy(id = "blibli.mobile.commerce:id/il_cart_logo")
    private WebElementFacade cartBtn;

    @FindBy(xpath = "//android.widget.TextView[@text='Masuk']")
    private WebElementFacade loginBtn;

    @FindBy(xpath = "//android.widget.FrameLayout[@content-desc = 'Kategori']")
    private WebElementFacade categoryBtn;

    @FindBy(xpath = "//android.widget.FrameLayout[@content-desc = 'Official Store']")
    private WebElementFacade officialStoreBtn;

    @FindBy(xpath = "//android.widget.TextView[@text='Semua']")
    private WebElementFacade allCategory;

    @FindBy(xpath = "//android.widget.RelativeLayout//android.widget.ImageView")
    private WebElementFacade popupCloseBtn;


    public void clickCartBtn() {
        cartBtn.withTimeoutOf(30, SECONDS).waitUntilVisible();
        cartBtn.click();
    }

    public Boolean isCartVisible() {
        return cartBtn.withTimeoutOf(30, SECONDS).isVisible();
    }

    public void clickCategoryBtn() {
        categoryBtn.
                withTimeoutOf(15, SECONDS).
                click();
    }

    public void clickOfficialStoreBtn() {
        officialStoreBtn.
                withTimeoutOf(15, SECONDS).
                click();
    }

    public void clickAllCategoryBtn(){
        allCategory.
                withTimeoutOf(30, SECONDS).
                click();
    }

    public void clickLoginBtn(){
        loginBtn.
                withTimeoutOf(30, SECONDS).
                click();
    }

    public void closePopUp(){
        popupCloseBtn.
                withTimeoutOf(30, SECONDS).
                click();
    }
}
