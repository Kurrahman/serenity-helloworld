package org.example.pages.MobilePages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

public class CheckoutPage extends PageObject {
    @FindBy(xpath = "//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_numberEntered']")
//    @FindBy(xpath = "(//android.widget.ScrollView//android.widget.TextView)[3]")
    private WebElementFacade phoneNumber;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_recharge_name']")
//    @FindBy(xpath = "(//android.widget.RelativeLayout[@resource-id='blibli.mobile.commerce:id/tv_numberEntered']//android.widget.TextView[@index='4'])")
    private List<WebElementFacade> productName;

    public String getPhoneNumber(){
        return phoneNumber.withTimeoutOf(10, SECONDS).getAttribute("text");
    }

    public String getProductName(){
        return productName.get(0).withTimeoutOf(10, SECONDS).getAttribute("text");
    }
}
