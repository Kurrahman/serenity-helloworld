package org.example.pages.MobilePages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

public class OfficialStorePage extends PageObject {
    @FindBy(xpath = "//android.widget.TextView[@content-desc = 'Cari']")
    WebElementFacade searchBtn;

    @FindBy(xpath = "//android.widget.EditText[@text = 'Kamu lagi cari apa?']")
    WebElementFacade searchBar;

    @FindBy(xpath = "//androidx.viewpager.widget.ViewPager[@id = 'blibli.mobile.commerce:id/rv_search_auto_complete']//android.view.ViewGroup[0]")
    WebElementFacade searchResults;

    public void search(String query) {
        searchBtn.
                withTimeoutOf(15, SECONDS).
                click();

        searchBar.
                withTimeoutOf(15, SECONDS).
                type(query);

        ((AndroidDriver) ((WebDriverFacade) getDriver()).getProxiedDriver()).pressKey(new KeyEvent(AndroidKey.ENTER));

//        searchResults.
//                withTimeoutOf(15, SECONDS).
//                click();
    }
}
