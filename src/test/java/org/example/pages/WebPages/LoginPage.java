package org.example.pages.WebPages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.example.utility.CheckingEmail;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

@DefaultUrl("https://www.blibli.com/login?redirect=%2F")
public class LoginPage extends PageObject {

    @FindBy(xpath = "//div[@class='blu-card']/div/div/div[1]//input")
    WebElementFacade emailForm;

    @FindBy(xpath = "//div[@class='blu-card']/div/div/div[2]//input")
    WebElementFacade passwordForm;

    @FindBy(xpath = "//div[@class='login__button']//button")
    WebElementFacade submitBtn;

    @FindBy(xpath = "//div[@class='otp-validation__confirm']/button[2]")
    WebElementFacade verifyByEmailBtn;

    @FindBy(xpath = "//ul[@class='otp__codeField']/li[1]/input")
    WebElementFacade otpForm;

    @FindBy(xpath = "//div[@class='otp__confirm']/button")
    WebElementFacade otpSubmitBtn;

    EnvironmentVariables properties = SystemEnvironmentVariables.createEnvironmentVariables();

    public void login(String email, String password) {
        emailForm.type(email);
        passwordForm.type(password);
        submitBtn.click();
        waitABit(3000);
    }

    public void fillEmail(String email) {
        emailForm.type(email);
    }

    public void fillPassword(String password) {
        passwordForm.type(password);
    }

    public void clickLogin() {
        submitBtn.click();
        waitABit(3000);
    }

    public void clickVerifyByEmail() {
        verifyByEmailBtn.click();
        waitABit(5000);
    }

    public void fillOtpCode() throws Exception {
        String otp = CheckingEmail.getVerificationCode(properties.getProperty("username"), properties.getProperty("password"));
        otpForm.sendKeys(otp);
    }

    public void clickSubmitOtp() {
        otpSubmitBtn.click();
    }

    public void openGmailOnNewTab() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_T);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_T);
        waitABit(3000);
//        getDriver().findElement(By.xpath("//body")).sendKeys(Keys.CONTROL+"t");
        ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
        getDriver().get("https://accounts.google.com/AccountChooser?service=mail&continue=https://mail.google.com/mail/");
    }
}
