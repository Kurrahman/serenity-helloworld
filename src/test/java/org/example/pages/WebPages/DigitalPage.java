package org.example.pages.WebPages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import java.util.List;

@DefaultUrl("https://www.blibli.com/digital/p/pulsa")
public class DigitalPage extends PageObject {

    @FindBy(xpath = "//span[@class='user__name'][1]")
    private WebElementFacade loginBtn;

    @FindBy(xpath = "//div[contains(text(),'Lanjut bayar')]")
//    @FindBy(xpath = "//button[@id='btn-paynow']")
    private WebElementFacade checkoutBtn;

    @FindBy(xpath = "//div[@class='tab']/div")
    private List<WebElementFacade> navigationBar;

    @FindBy(xpath = "//input[@class='form__input']")
    private WebElementFacade formHandphone;

    @FindBy(xpath = "//button[@tabindex='-1']")
    private WebElementFacade clearHandphoneForm;

    @FindBy(xpath = "//div[@class='select-product']")
    private List<WebElementFacade> products;

    @FindBy(xpath = "//div[@class='tabs-digital__header']/a[1]")
    private WebElementFacade subProductPulsa;

    @FindBy(xpath = "//div[@class='tabs-digital__header']/a[2]")
    private WebElementFacade subProductPaketData;

    @FindBy(xpath = "//div[@class='tabs-digital__header']/a[3]")
    private WebElementFacade subProductRoaming;

    @FindBy(xpath = "//span[@class='blu-field__msg']")
    private WebElementFacade errorMsg;

    public void fillNumber(String number) {
        formHandphone.typeAndTab(number);
//        waitABit(1000);
    }

    public void selectProduct(String productName) {
        for (WebElementFacade product :
                products) {
            if (product.getText().contains(productName)) {
                product.click();
                return;
            }
        }
    }

    public void changeInputNumber(int ndigit, int position, String change) {
        formHandphone.click();
        formHandphone.sendKeys(Keys.HOME);
        for (int i = 0; i < position; i++) {
            formHandphone.sendKeys(Keys.ARROW_RIGHT);
        }
        for (int i = 0; i < ndigit; i++) {
            formHandphone.sendKeys(Keys.DELETE);
        }
        formHandphone.sendKeys(change);
//        waitABit(1000);
    }

    public void clickLoginBtn() {
        loginBtn.click();
//        waitABit(3000);
    }

    public void checkout() {
//        checkoutBtn.click();
        formHandphone.sendKeys(Keys.ENTER);
//        waitABit(3000);
    }

    public void moveToPaketData() {
        subProductPaketData.click();
    }

    public void moveToPulsa() {
        subProductPulsa.click();
    }

    public void moveToRoaming() {
        subProductRoaming.click();
    }

    public boolean isErrorNumberVisible() {
        return errorMsg.isVisible();
    }

    public void submitNumber(){
        formHandphone.sendKeys(Keys.ENTER);
    }

}
