package org.example.pages.WebPages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class CheckoutPage extends PageObject {
    @FindBy(xpath = "//div[@class='pulsa__summary-value']")
    private List<WebElementFacade> summaryValue;

    @FindBy(xpath = "//div[@class='pulsa__summary-row'][1]/div[@class='pulsa__summary-value']")
    private WebElementFacade handphoneNumber;

    @FindBy(xpath = "//div[@class='pulsa__summary-row'][3]/div[@class='pulsa__summary-value']")
    private WebElementFacade productName;

    @FindBy(xpath = "//button[@class='payment__buyNow']")
    private WebElementFacade payBtn;

    public String getHandphoneNumber() {
        waitABit(2000);
        return handphoneNumber.getText();
    }

    public String getProductName() {
        waitABit(2000);
        return productName.getText();
    }

    public void payNow() {
        payBtn.click();
    }

    public boolean buttonVisible() {
        return payBtn.isVisible();
    }
}
