package org.example.controller;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import org.example.response.*;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.response.Response;
import org.example.utility.CookieLoader;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class APIController {
    private String BaseURL = "https://www.blibli.com/backend/";
    private static Cookies cookies = new Cookies();

    private RequestSpecification requestHeader() {
        return SerenityRest.given().
                cookie(CookieLoader.getQAKey()).
                header("accept", "*/*").
                header("Content-type", "application/json").
                header("Accept-Encoding", "gzip, deflate, br").
                header("User-Agent", "PostmanRuntime/7.25.0").
                header("Connection", "keep-alive").when();
    }

    public NumberFormResponse phoneNumberQuery(String phoneNumber, String product) {
        String productType = "digital-product/products?productType=" + product + "&customerNumber=";
        String URL = BaseURL + productType + phoneNumber;
        Response response = requestHeader().get(URL);
//        updateCookie(response);
//        System.out.println(response.prettyPrint());
        return response.getBody().as(NumberFormResponse.class);
    }

    public LoginResponse login(String email, String password) {
        String URL = BaseURL + "common/users/_login";
        String body = "{\"username\":\"" + email + "\",\"password\":\"" + password + "\"}";
        Response response = requestHeader().body(body).post(URL);
        updateCookie(response);
//        System.out.println(response.getBody().prettyPrint());
        return response.getBody().as(LoginResponse.class);
    }

    public AddCartResponse addToCart(String customerNumber, String sku, String operatorName, String productType) {
        String URL = BaseURL + "digital-product/carts/_customer-number";
        String body = "{" +
                "\"customerNumber\":\"" + customerNumber + "\"," +
                "\"sku\":\"" + sku + "\"," +
                "\"operatorName\":\"" + operatorName + "\"," +
                "\"productType\":\"" + productType + "\"" +
                "}";
//        System.out.println(cookies);
        Response response = requestHeader().cookies(cookies).body(body).post(URL);
//        updateCookie(response);
        return response.getBody().as(AddCartResponse.class);
    }

    public GetCartResponse getUserCart() {
        String URL = BaseURL + "digital-product/carts";
        Response response = requestHeader().cookies(cookies).get(URL);

//        System.out.println(response.prettyPrint());
        return response.as(GetCartResponse.class);
    }

    public GetCartResponse setPaymentMethod(String paymentMethod) {
        String URL = BaseURL + "digital-product/carts/_payment";
        String body = "{\"paymentMethod\":\"" + paymentMethod + "\"}";
//        System.out.println(body);
        Response response = requestHeader().cookies(cookies).put(URL);
//        updateCookie(response);
        return response.getBody().as(GetCartResponse.class);
    }

    public RequestResponse cancelOrder(String orderId) {
        String URL = BaseURL + "digital-product/orders/_cancel-order";
        String body = "{\"orderId\":\"" + orderId + "\"}";

        Response response = requestHeader().cookies(cookies).body(body).post(URL);
//        updateCookie(response);
        return response.getBody().as(RequestResponse.class);
    }

    public CheckoutResponse checkout() {
        String URL = BaseURL + "digital-product/orders";
        String body = "{\"extendedData\": {\"PAYMENT_ACTION\": \"NORMAL\"}}";

        Response response = requestHeader().cookies(cookies).body(body).post(URL);
//        updateCookie(response);
//        System.out.println(response.prettyPrint());
        return response.getBody().as(CheckoutResponse.class);
    }

    public void requestChallengeCode(String token) {
        String URL = BaseURL + "common/users/_request-challenge-code";
        String body = "{\"challenge\":{\"token\":\"" + token + "\"},\"type\":\"EMAIL\"}";

        Response response = requestHeader().cookies(cookies).body(body).post(URL);
//        updateCookie(response);
    }

    public LoginResponse loginWithOTP(String token, String otp, String email) {
        String URL = BaseURL + "common/users/_login";
        String body = "{\"challenge\":{\"token\":\"" +
                token +
                "\",\"code\":\"" +
                otp +
                "\"},\"username\":\"" +
                email +
                "\"}";
        Response response = requestHeader().cookies(cookies).body(body).post(URL);
        updateCookie(response);
        return response.getBody().as(LoginResponse.class);
    }

    private void updateCookie(Response response) {
        Cookies tmp = response.getDetailedCookies();
        List<Cookie> tmpC = new ArrayList<>();
        String[] cookieNames = {
                "Blibli-User-Id",
                "Blibli-Is-Member",
                "Blibli-Is-Remember",
                "Blibli-Session-Id",
                "Blibli-Signature",
                "Blibli-Device-Id",
                "Blibli-Device-Id-Signature"
        };
        for (int i = 0; i < cookieNames.length; i++) {
            if (tmp.get(cookieNames[i]) != null) {
                tmpC.add(tmp.get(cookieNames[i]));
            }
        }
        cookies = new Cookies(tmpC);
//        System.out.println(cookies.toString());
//        System.out.println();
    }

    public Cookies getCookies() {
        List<Cookie> tmpC = new ArrayList<>(cookies.asList());
        tmpC.add(CookieLoader.getQAKey());
        return new Cookies(tmpC);
    }

    public void setCookies(Cookies cookie){
        cookies = cookie;
    }

    public static void main(String[] args) {
        APIController apiController = new APIController();
        LoginResponse loginResponse = apiController.login("akutakbiasa36@gmail.com", "random123");
        System.out.println(apiController.getCookies().toString());
//        System.out.println(apiController.setPaymentMethod("VirtualAccountBcaOnline").getData().getItem().getName());
    }
}
