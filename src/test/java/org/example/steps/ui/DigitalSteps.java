package org.example.steps.ui;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.example.pages.WebPages.CheckoutPage;
import org.example.pages.WebPages.DigitalPage;

import static org.hamcrest.MatcherAssert.assertThat;

public class DigitalSteps {

    DigitalPage digitalPage;
    CheckoutPage checkoutPage;

    @When("^The user fill (.*) to the form$")
    public void theUserFillHandphoneToTheForm(String number) {
        digitalPage.fillNumber(number);
    }

    @When("^The user change (\\d+) from position (\\d+) to (.*)$")
    public void theUserChangeNdigitFromPositionToChange(int n, int pos, String change) {
        digitalPage.changeInputNumber(n, pos, change);
    }

    @When("^The user change the product to (.*)$")
    public void theUserChangeTheAmountToAmount(String productName) {
        digitalPage.selectProduct(productName);
    }

    @Then("^The user should not be able to continue to checkout$")
    public void theUserShouldNotBeAbleToContinueToCheckout() {
        digitalPage.checkout();
        assertThat("The number filled considered valid", !checkoutPage.buttonVisible());
    }

    @When("^The user click on pay now button$")
    public void theUserClickOnPayNowButton() {
        digitalPage.checkout();
    }

    @And("^The user change product to paket data$")
    public void theUserChangeProductToPaketData() {
        digitalPage.moveToPaketData();
    }

    @Then("^The user should see an error message$")
    public void theUserShouldSeeAnErrorMessage() {
        assertThat("Error message is not visible", digitalPage.isErrorNumberVisible());
    }

    @When("^The user click on login button$")
    public void theUserClickOnLoginButton() {
        digitalPage.clickLoginBtn();
    }

    @When("^The user submit the number$")
    public void theUserSubmitTheNumber() {
        digitalPage.submitNumber();
    }
}
