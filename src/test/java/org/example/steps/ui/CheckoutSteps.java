package org.example.steps.ui;

import cucumber.api.java.en.Then;
import org.example.pages.WebPages.CheckoutPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CheckoutSteps {
    CheckoutPage checkoutPage;

    @Then("^The checkout amount should be (.*)$")
    public void theCheckoutAmountShouldBeAny(String amount) {
        assertThat("The amount don't match", checkoutPage.getProductName(), equalTo(amount));
    }

    @Then("^The checkout number should be (.*)$")
    public void theCheckoutNumberShouldBeHandphone(String handphone) {
        assertThat("The handphone number don't match", checkoutPage.getHandphoneNumber(), equalTo(handphone));
    }

}
