package org.example.steps.ui;

import cucumber.api.java.en.When;
import org.example.pages.WebPages.LoginPage;

import static org.hamcrest.MatcherAssert.assertThat;

public class LoginSteps {

    private static final String ACCOUNT_NAME = "//div[@class='account__text']";

    LoginPage loginPage;

    @When("^The user fill username with \"([^\"]*)\" to the login form$")
    public void theUserFillUsernameWithToTheLoginForm(String email) throws Throwable {
        loginPage.fillEmail(email);
    }

    @When("^The user fill password with \"([^\"]*)\" to the login form$")
    public void theUserFillPasswordWithToTheLoginForm(String password) throws Throwable {
        loginPage.fillPassword(password);
    }

    @When("^The user click the submit button on login form$")
    public void theUserClickTheSubmitButtonOnLoginForm() {
        loginPage.clickLogin();
    }

    @When("^The user click on verification by email button$")
    public void theUserClickOnVerificationByEmailButton() {
        loginPage.clickVerifyByEmail();
    }

    @When("^The user fill OTP code$")
    public void theUserFillOTPCode() throws Exception {
        loginPage.fillOtpCode();
    }

    @When("^The user click submit OTP button$")
    public void theUserClickSubmitOTPButton() {
        loginPage.clickSubmitOtp();
    }
}
