package org.example.steps.ui;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.example.utility.CookieLoader;
import org.example.pages.WebPages.DigitalPage;
import org.example.pages.WebPages.LoginPage;

import static org.example.utility.CookieLoader.*;

public class GeneralSteps {
    DigitalPage digitalPage;

    LoginPage loginPage;

    @Given("^The user is on the login page$")
    public void goToHomepage() {
        loginPage.open();
    }

    @Given("^The user logging in with '(.*)' and '(.*)' on digital page$")
    public void loginInDigitalPage(String email, String password) {
        digitalPage.clickLoginBtn();
        loginPage.login(email, password);
    }

    @Given("^The user logging in with '(.*)' and '(.*)' on login page$")
    public void loginInLoginPage(String email, String password) {
        loginPage.login(email, password);
    }

    @Given("^The user is on the digital product page$")
    public void theUserInOnTheDigitalProductPage() {
        digitalPage.open();
    }

    @Given("^The QA cookie is loaded to the browser$")
    public void theQACookieIsLoadedToTheBrowser() {
        CookieLoader.loadCookie(COOKIE_QA_DIRECTORY, digitalPage.getDriver());
        digitalPage.getDriver().navigate().refresh();
    }

    @And("^The Login cookie is loaded to the browser$")
    public void theLoginCookieIsLoadedToTheBrowser() {
        CookieLoader.loadCookie(COOKIE_LOGIN_DIRECTORY, digitalPage.getDriver());
//        digitalPage.getDriver().navigate().refresh();
        digitalPage.open();
    }
}
