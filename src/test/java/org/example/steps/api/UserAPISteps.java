package org.example.steps.api;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.example.controller.APIController;
import org.example.response.CheckoutResponse;
import org.example.response.GetCartResponse;
import org.example.response.LoginResponse;
import org.example.utility.CheckingEmail;
import org.example.utility.CookieLoader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class UserAPISteps {
    APIController apiController = new APIController();
    EnvironmentVariables properties = SystemEnvironmentVariables.createEnvironmentVariables();
    CheckoutResponse checkoutResponse;
    LoginResponse loginResponse;
    private static String challengeToken;
    private static String otp;

    @Given("^User login using \"([^\"]*)\" with password \"([^\"]*)\"$")
    public void userLoginUsingWithPassword(String email, String password) {
        LoginResponse response = apiController.login(email, password);
        challengeToken = response.getData().getChallenge().getToken();
    }

    @Given("^User request challenge code$")
    public void userRequestChallengeCode() {
        apiController.requestChallengeCode(challengeToken);
    }

    @Given("^User retrieve OTP code from \"([^\"]*)\" email inbox$")
    public void userRetrieveOTPCode(String email) throws Exception {
        Thread.sleep(10000);
        otp = CheckingEmail.getVerificationCode(properties.getProperty("username"), properties.getProperty("password"));
    }

    @When("^User submit the OTP code$")
    public void userSubmitOTPCode() {
        loginResponse = apiController.loginWithOTP(challengeToken, otp, properties.getProperty("username"));
    }

    @When("^User checkout the current cart$")
    public void userCheckoutTheCurrentCart() {
        checkoutResponse = apiController.checkout();
    }

    @Then("^The checkout response should be 200 OK$")
    public void theCheckoutResponseShouldBeOK() {
        assertThat("Response code is not 200", checkoutResponse.getCode(), equalTo(200));
    }

    @Then("^User should be logged in$")
    public void userShouldBeLoggedIn() {
        assertThat("Response code is not 200", loginResponse.getCode(), equalTo(200));
        CookieLoader.writeCookies(apiController.getCookies());
    }

    @Given("^User load cookies from file$")
    public void userLoadCookiesFromFile() {
        apiController.setCookies(CookieLoader.getLoginCookies());
    }

    @Then("^An id for the order is generated$")
    public void anIdForTheOrderIsGenerated() {
        assertThat("The order id is not generated", checkoutResponse.getData().getOrderId() != null);
    }
}
