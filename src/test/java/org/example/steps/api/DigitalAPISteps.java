package org.example.steps.api;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.example.controller.APIController;
import org.example.response.AddCartResponse;
import org.example.response.NumberFormResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DigitalAPISteps {
    APIController apiController = new APIController();
    NumberFormResponse numberFormResponse;
    AddCartResponse addCartResponse;

    @Given("^User request for phone credit for (.*)$")
    public void requestPhoneCredit(String phoneNumber) {
        numberFormResponse = apiController.phoneNumberQuery(phoneNumber, "PHONE_CREDIT");
    }

    @Given("^User request for data package for (.*)$")
    public void requestDataPackage(String phoneNumber) {
        numberFormResponse = apiController.phoneNumberQuery(phoneNumber, "DATA_PACKAGE");
    }

    @When("^The number response should be 200 OK$")
    public void numberResponse200OK() {
        assertThat("Response code is not 200", numberFormResponse.getCode(), equalTo(200));
    }

    @Then("^The provider should be (.*)$")
    public void providerName(String provider) {
        assertThat("Provider name is not matched", numberFormResponse.getData().getProvider(), equalTo(provider));
    }

    @When("^The number response should be 429 TOO_MANY_REQUEST$")
    public void response429() {
        assertThat("Response code is not 429", numberFormResponse.getCode(), equalTo(429));
    }

    @When("^User add order (.*), (.*), (.*), and \"([^\"]*)\" to cart$")
    public void addOrder(String customerNumber, String sku, String operatorName, String productType) {
        addCartResponse = apiController.addToCart(customerNumber, sku, operatorName, productType);
    }

    @Then("^The add to cart response should be 200 OK$")
    public void theDataPackageCartResponseShouldBeOK() {
        assertThat("Response code is not 200", addCartResponse.getCode(), equalTo(200));
    }


    @Then("^The sku added should be (.*)$")
    public void theSkuAddedShouldBeSku(String sku) {
        assertThat("The item's sku did not match", addCartResponse.getData().getItem().getSku(), equalTo(sku));
    }
}
