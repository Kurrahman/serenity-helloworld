package org.example.steps.mobile;

import cucumber.api.java.en.Given;
import org.example.pages.MobilePages.*;


public class MobileGeneralSteps {
    HomePage homePage;
    LoginPage loginPage;
    AllCategoryPage allCategoryPage;

    @Given("^User open Blibli application$")
    public void userOpenBlibliApplication() {
//        homePage.closePopUp();
        homePage.clickLoginBtn();
    }

    @Given("^User click login using google$")
    public void userClickLoginUsingGoogle() {
        loginPage.loginByGoogleAccount();
    }

    @Given("^User select the topmost google account$")
    public void userSelectTheTopmostGoogleAccount() {
        loginPage.selectGoogleAccount();
    }

    @Given("^User click on category button$")
    public void userClickOnCategoryButton() {
        homePage.clickCategoryBtn();
    }

    @Given("^User click on all category button$")
    public void userClickOnAllCategoryButton() {
        homePage.clickAllCategoryBtn();
    }

    @Given("^User click on \"([^\"]*)\" Category$")
    public void userClickOnCategory(String category) throws Throwable {
        allCategoryPage.clickCategoryBtn(category);
    }
}
