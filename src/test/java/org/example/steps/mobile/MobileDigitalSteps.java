package org.example.steps.mobile;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.example.pages.MobilePages.CheckoutPage;
import org.example.pages.MobilePages.DigitalPage;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MobileDigitalSteps {
    DigitalPage digitalPage;
    CheckoutPage checkoutPage;

    @When("^User fill (.*) to the form$")
    public void userFillNumberToTheForm(String number) {
        digitalPage.fillPhoneNumber(number);
    }

    @Then("^User should see an error message$")
    public void userShouldSeeAnErrorMessage() {
        assertThat("Error message is not shown", digitalPage.isErrorVisible());
    }

    @When("^User click on pay now button$")
    public void userClickOnPayNowButton() {
        digitalPage.clickCheckoutBtn();
    }

    @Then("^Checkout number should be (.*)$")
    public void checkoutNumberShouldBeHandphone(String expected) {
        assertThat("The phone number does not match", checkoutPage.getPhoneNumber(), equalTo(expected));
    }

    @When("^User click on products dropdown$")
    public void userClickOnProductsDropdown() {
        digitalPage.clickValueDropdown();
    }

    @When("^User change the product to (.*)$")
    public void userChangeTheProductToProductName(String productName) {
        digitalPage.selectValue(productName);
    }

    @Then("^Checkout product name should be (.*)$")
    public void checkoutProductNameShouldBeProductName(String expected) {
        assertThat("The product name does not match", checkoutPage.getProductName(), equalTo(expected));
    }
}
