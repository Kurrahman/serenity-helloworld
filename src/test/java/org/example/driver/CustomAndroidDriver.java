package org.example.driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import lombok.SneakyThrows;
import net.serenitybdd.core.SerenitySystemProperties;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

public class CustomAndroidDriver implements DriverSource {

    @SneakyThrows
    @Override
    public WebDriver newDriver() {
        EnvironmentVariables properties = SystemEnvironmentVariables.createEnvironmentVariables();
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appium:automationName", "uiautomator2");
        capabilities.setCapability("appium:udid", properties.getProperty("udid"));
        capabilities.setCapability("appPackage", "blibli.mobile.commerce");
        capabilities.setCapability("appActivity", "blibli.mobile.ng.commerce.core.init.view.SplashActivity");
        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");
        return new AndroidDriver<>(new URL(properties.getProperty("endpoint")), capabilities);
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}
